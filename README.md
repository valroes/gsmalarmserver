#GSM ALARM SERVER

Installation
============
1. Es wird eine externe python library benötigt, diese muss zuerst installiert werden via: `sudo apt install python3-schedule`

2. Der Server muss als systemd Service aufgesetzt werden, damit er beim hochfahren automatisch gestarted wird. Zudem wird das Programm so auch neu gestarted, falls es aus irgendeinem Grund abstürtzen sollte. Um das Program als Service zu konfigurieren:

2.1 Das Service file an den richtigen Ort kopieren: `cp gsmalarmserver/setup/gsm-alarm.service /lib/systemd/system`
2.2 Den daemon neu laden: `sudo systemctl daemon-reload`
2.3 Den Service konfigurieren, dass er beim hochfahren automatisch ausgeführt wird: `sudo systemctl enable gsm-alarm.service`

Alternative kannst du probrieren, dass script gsmalarmserver/setup/make_service.sh auszuführen, dieses sollte Schritte 2.1-2.3 automatisch ausfüheren.

Starten
=======
Das Programm kann auf verschiedene Weiesen gestarted werden:
1. Über systemctl mit: `sudo systemctl start gsm-alarm.service`
2. Durch Ausführen des Scripts gsmalarmserver/start_alarm_server.sh (script führt den Befehl von 1 aus).
3. Direkt mit python. Hier ist es wichtig, dass du im Ordner gsmalarm server bist. Dann `python3 src/alarm_server.py`. Es sollte vorher sichergestellt werden, dass das Programm nicht bereits als Service läuft (`systemctl status gsm-alarm.service`).


Stoppen
=======
Auch zum Stoppen des Programms gibt es verschiedene Optionen:
1. Übersystemctl mit: `sudo systemctl stop gsm-alarm.service`
2. Durch Ausführen des Scripts gsmalarmserver/stop_alarm_server.sh (script führt den Befehl von 1 aus).
3. Mit hilfe der Stoptaste: Kurz drücken beendet Programm. Wenn der Knopf für 3sec gedrückt wird, dann fährt das RPI herunter. 
4. Es kann auch manuel ein SIGTERM Befhel an das Programm gesendet werden z.b. mit htop

Logging
=======
Es wird jeweils ein log angelegt im Ordner gsmalarmserver/logs. Im config file kann spezifiziert werden, ab welchem "Schweregrad" einer Information diese ins log file geschrieben werden soll.
Der Status des Services kann erfragt werden via: `systemctl status`
Wenn das Programm direkt mit Python ausgeführt wird, so werden Informationen direkt in die Konsole geprintet. Auch hier kann der Detail Level im config file bestimmt werden.
