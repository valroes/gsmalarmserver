from logging import DEBUG, INFO, WARNING, ERROR, CRITICAL
from alarm import Alarm

#--Alarm Setup-------------------------------------------------------
ALARMS = [Alarm(5,  "Alarm Text 1", ["+41764154494"]),
          Alarm(6,  "Alarm Text 2", ["+41764154494"]),
          Alarm(13, "Alarm Text 3", ["+41764154494"]),
          Alarm(19, "Alarm Text 4", ["+41764154494"]),
          Alarm(26, "Alarm Text 5", ["+41764154494"]),
          Alarm(29, "Alarm Text 6", ["+41764154494"])
          ]

#--Pin Setup---------------------------------------------------------
ALARM_PINS = [el.pin for el in ALARMS] #Holt die Pins von den Alarmen

STOP_PIN       = 24
LED_PIN_RED    = 20
LED_PIN_YELLOW = 16
GSM_PIN_POWER  = 12
GSM_PIN_FLIGHT = 4

#--Lebenszeichen Setup-----------------------------------------------
LIFE_SIGNAL_NUMBER = "+41764154494"
LIFE_SIGNAL_TIME   = "16:00"
LIFE_SIGNAL_FREQ   = 1 #in Tagen

#--Logging Setup-----------------------------------------------------
CONSOLE_LEVEL = DEBUG
FILE_LEVEL    = INFO
