import time
import threading
import signal
import schedule

import gsm
import rpi
import logger
from config import *



class AlarmServer:
    def __init__(self):
        """ Diese Klasse ist das Hauptprogram. Von hier aus werden alle anderen
            Module initialisiert. Im Mainloop dieser Klasse wird der Knopf ueberwacht
            und die periodische Sendung eines Lebenszeichens verwaltet. Ausserdem
            bietet die Klasse interface Funktionen um mit dem Betriebssystem zu
            sprechen.
        """
        self.running       = True
        self.stop_pin      = STOP_PIN
        self.stopping      = False
        self.stop_pressed  = False
        self.stop_released = False
        self.stop_time     = None
        self.hard_stop     = False
        self.threads       = list()

        self.logger = logger.Logger("AlarmServer").get()
        self.rpi = rpi.RPI(self)
        self.sms = gsm.SIM7600(self.rpi)
        self.alarms = {el.pin: el for el in ALARMS}

        signal.signal(signal.SIGTERM, self.on_sigterm)
        schedule.every(LIFE_SIGNAL_FREQ).days.at(LIFE_SIGNAL_TIME).do(self.send_life_signal)
        self.sms.send_greeting(LIFE_SIGNAL_NUMBER)

    def __exit__(self, ty, val, tb):
        """ Diese Funktion wird automatisch aufgerufen, wenn das Program auf
            irgendeine Art beendet wird, also auch bei einem Program Absturz. Die
            Funktion probiert alle Prozesse sauber zu beenden, bevor das Program
            verlassen wird.

            Args:
                ty(type): Der type des Fehlers.
                val(Exception): Welche Exception ausgelöst wurde.
                tb(traceback): Ein Report mit Information die beim debuggen helfen.
        """
        if ty == KeyboardInterrupt:
            self.logger.info("Das Programm wurde mit CTRL C beendet")
        elif ty == SystemExit:
            self.logger.info("Das Programm wurde mit dem Stop Knopf beendet")
        elif ty:
            self.logger.critical("Das Programm ist unerwarted abgestuerzt.")
            self.logger.critical(f"Type: {ty} Value: {val} Traceback: {tb}")
        self.stop()
        self.logger.info("Programm beendet")

    def __enter__(self):
        """ Wird benötigt, damit die Klasse mit dem 'with as' Kontext Manager läuft.

            Returns: Die Object Instanz von sich selber.
        """
        return self

    def run(self):
        """ Diese Funktion stösst das programm an. Die Sub module werden
            initialisiert und gestarted. Ein thread fuer jedes Modul wird erstellt.
            Danach geht das Programm in den Mainloop ueber.
        """
        self.threads.append(threading.Thread(target=self.rpi.main_loop))
        self.threads.append(threading.Thread(target=self.sms.main_loop))
        for el in self.threads:
            el.start()

        self.start_time = time.time()
        self.main_loop()

    def stop(self):
        """ Diese Funktion beendet das Program. Die Submodule werden gestoppt.
            Diese Funktion wird auch beim betätigen des Knopfes ausgelöst. Wurde der
            Knopf nur kurz betätigt wird der Stromspar modus deaktiviert. Wenn der
            Knopf gehalten wurde, wird das RPI herunter gefahren.
        """
        self.running = False
        self.sms.stop()
        self.rpi.stop()
        self.rpi.power_save_off()

        if self.hard_stop:
            self.logger.info("Herunterfahren verlangt")
            #self.rpi.power_off()

    def main_loop(self):
        """ Der Mainloop des Programms. Hier wird der Lebenszeichen Scheduler
            geupdated und der Knopf ueberwacht.
        """
        while self.running:
            schedule.run_pending()
            if self.stopping:
                self.eval_stop()
            time.sleep(0.1)

    def on_sigterm(self, signum, frame):
        """ Diese Funktion wird ausgefuehrt, wenn das Program ein spezielles Signal
            erhält (SIGTERM). Dieses Signal wird vom OS verwendet um Programme zu
            beenden. Wenn du z.B. ein Programm mit dem Taskmanager beendest wird
            dieses Signal and das Programm geschickt. systmctl welches verwenedet
            wird um dieses Program zu steuern arbeitet auch mit diesen Signalen.
            Wen das Signal erhalten wird, wird das Program beendet.
        """
        self.logger.info("SIGTERM signal erhalten, beende das Programm")
        self.stop()


    def eval_stop(self):
        if time.time() - self.stop_time > 3:
            self.hard_stop = True
            exit()

    def send_life_signal(self):
        """ Diese Funktion schickt ein Lebenszeichen, wenn deie spezifizierte Zeit
            verstrichen ist.
        """
        self.logger.info(f"Sende Lebenszeichen an {LIFE_SIGNAL_NUMBER}")
        self.sms.send(LIFE_SIGNAL_NUMBER, "Lebenszeichen")

    def send_alarm(self, alarm):
        """ Schickt einen alarm an alle Nummern die ihn erhalten sollen.

            Args:
                alarm(alarm.Alarm): Das entsprechende Alarm object.
        """
        for number in alarm.numbers:
            self.logger.info(f"Sende alarm '{alarm.text}' an{number}")
            self.sms.send(number, alarm.text)

    def report_pin_change(self, pin_nmb, state):
        """ Diese Funktion wird vom RPI modul aufgerufen, wenn sich der Status eines
            Input Pins verändert hat. Wenn es sich um einen Alarm Pin handelt, wird
            der entsprechende Alarm ausgelöst. Wenn es der Stop Knopf pin war, wird
            eine Routine ausglöst um zu bestimmen wie lang er gehalten wird und die
            entsprechenden funktionen auszulösen.

            Args:
                pin_nmb(int): Die Nummer des Pins, welcher den Status gewechselt hat.
                state(int): Der neue Status des entsprechenden Pins (1=high, 0=low).
        """
        self.logger.debug(f"Pin Nummer {pin_nmb} hat Status gewechselt auf {state}")

        if pin_nmb in self.alarms:
            if state == 1:
                self.send_alarm(self.alarms[pin_nmb])

        elif pin_nmb == self.stop_pin:
            if not self.stopping:
                self.rpi.start_blink_alternate(0.1)
                self.stop_time = time.time()
                self.stopping = True
            else:
                self.running = False
                exit()


#Hier wird das Program gestarted.
if __name__ == "__main__":
    with AlarmServer() as alarm_server:
        alarm_server.run()


