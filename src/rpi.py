import os
import time
import threading
import RPi.GPIO as GPIO

import logger
from config import *

class DO:
    def __init__(self, nmb):
        """ Diese Klasse ist ein Container für alle GPIOs die als output konfiguriert
            sind. Sie soll den Zugriff auf diese Pins etwas vereinfachen und
            übersichtlicher machen.

            Args:
                nmb(int): Die Pin Nummer des Outputs.
        """
        self.nmb = nmb          #Die nummer des pins
        self.state = False

        GPIO.setup(self.nmb, GPIO.OUT)
        self.turn_off()


    def set_state(self, state):
        """ Setzt den Output des entsprechenden GPIOs.

            Args:
                State(bool): Der gewünschte State auf den geschalten wird.
        """
        if state == True or state == 1:
            GPIO.output(self.nmb, GPIO.HIGH)
            self.state = True
        else:
            GPIO.output(self.nmb, GPIO.LOW)
            self.state = False

    def turn_on(self):
        """ Setzt den Output auf High."""
        self.set_state(True)

    def turn_off(self):
        """ Setzt den Output auf Low."""
        self.set_state(False)

    def toggle(self):
        """ Wechseslt den Output auf das inverse des momentanen Zustandes."""
        self.set_state(not self.state)




class DI:
    def __init__(self, nmb):
        """ Diese Klasse ist eine Abstraktion aller als Input konfigurierten GPIOs.
            Das Object merkt siche seinen letzten state und vereinfacht den Zugriff
            auf die Pins.

            Args:
                nmb(int): Die Pin Nummer des Inputs.
        """
        self.nmb = nmb          #Die nummer des pins

        GPIO.setup(self.nmb, GPIO.IN)
        self.last_cycle = 0     #Für Prellschutz
        self.last_reported = GPIO.input(self.nmb) #Zuletz raportierter state
        self.up_to_date = True  #Wurde ein neuer state schon raportiert


    def get_state(self):
        """ Check ob der pin high oder low ist. Implementiert ebenfalls Prellschutz
            und checked, ob wenn ein neuer Wert gelesen wird, dieser schon raportiert
            wurde.

            Returns:
                state(int): Der momentane status des pins.
        """
        state = GPIO.input(self.nmb)
        if state != self.last_reported and state == self.last_cycle:
            self.up_to_date = False
        return state

    def update(self, state):
        """Feedback von DIO, dass der neue state dem master gemeldet wurde.

        Args:
            state(int): Der state welcher dem master raportiert wurde.
        """
        self.up_to_date = True
        self.last_reported = state

    @property
    def state(self):
        """ Über dieses Attribut kann der Momentane Zustand abgeholt werden."""
        return GPIO.input(self.nmb)


class RPI:
    def __init__(self, master):
        """ Diese Klasse dient als interface zu allen relevanten Funktionen des RPIs.
            Hier werden die GPIOs verwaltet und angesteuert. So wird das blinken der
            LEDs Konfigurieret und der Status der Alarm Inputs überwacht. Ausserdem
            stellt diese Klassen OS funktionen zur Verfügung, die das Ausschalten
            des Geräts ermöglichen oder das umschalten, des Stromspaar Modus.

            Args:
                master(alarm_server.AlarmServer): Die Instanz, des Alarm Servers.

        """
        self.master = master
        self.running = True

        self.blink_thread = None
        self.should_blink = False
        self.logger = logger.Logger("RPI").get()

        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        self.power_save_on()

        self.di_nmbs = ALARM_PINS + [STOP_PIN]
        self.di = list()
        for nmb in self.di_nmbs:
            self.di.append(DI(nmb))


        self.leds = {"red": DO(LED_PIN_RED), "yellow": DO(LED_PIN_YELLOW)}
        self.gsm_pins = {"power": DO(GSM_PIN_POWER), "flightmode":DO(GSM_PIN_FLIGHT)}

        self.logger.info("Instanz gestarted")
        self.default_blink()


    def stop(self):
        """ Stopt das RPI Modul."""
        self.logger.info("Instanz wird gestoppt")
        self.running = False
        if self.blink_thread:
            self.stop_blink()
        for key in self.leds:
            self.leds[key].turn_off()
        GPIO.cleanup()
        self.logger.info("Instanz beendet")


    def main_loop(self):
        """ Die Hauptschleife des Moduls. Hier werden kontinuierlich alle Inputs
            abgefragt. Falls deren Zustand sich verändert hat, wird dies dem
            Alarm Server mitgeteilt.
        """
        while self.running:

            for pin in self.di:
                state = pin.get_state()
                if not pin.up_to_date:
                    self.master.report_pin_change(pin.nmb, state)
                    pin.update(state)

                pin.last_cycle = state
            time.sleep(0.1) #Diese Dauer bestimmt Prellschutz

    def get_pin(self, pin_nmb):
        """ Hilfs Funktion welche es erlaubt, ein DI Object nach seiner Nummer zu
            suchen.

            Args:
                pin_nmb(int): Die Nummer des gesuchten Input Pins.

            Returns: Das DI Object, welches zu der spezifizierten Nummer gehört.
        """
        for pin in self.di:
            if pin.nmb == pin_nmb:
                return pin

    #--------------------------------------------------------------------------------
    # RPI Control
    #--------------------------------------------------------------------------------
    def power_save_on(self):
        """ Schaltet den Stromsparmodus des RPIs ein."""
        self.logger.info("Stromsparmodus eingeschalted")
        os.system("echo '1-1' | sudo tee /sys/bus/usb/drivers/usb/unbind > /dev/null")
        os.system("sudo /opt/vc/bin/tvservice -o > /dev/null")

    def power_save_off(self):
        """ Schaltet den Stromsparmodus des RPIs aus."""
        self.logger.info("Stromsparmodus ausgeschalted")
        os.system("echo '1-1' | sudo tee /sys/bus/usb/drivers/usb/bind > /dev/null")
        os.system("sudo /opt/vc/bin/tvservice -p > /dev/null")
        os.system("sudo /bin/chvt 6 > /dev/null; sudo /bin/chvt 7 > /dev/null")

    def power_off(self):
        """ Fährt das RPI hinunter."""
        self.logger.info("Raspberry fährt herunter")
        os.system("sudo shutdown -h now")

    #--------------------------------------------------------------------------------
    # LED Control
    #--------------------------------------------------------------------------------
    def start_blink_alternate(self, t=0.5):
        """ Lässt beide LEDs alternierend blinken.

            Args:
                t(float): Die Zeit nach der vom einen auf das andere LED gewechselt
                    wird.
        """
        if self.blink_thread:
            self.stop_blink()
        self.should_blink = True
        self.blink_thread = threading.Thread(target=self.do_blink_alternate, args=(t,))
        self.blink_thread.start()

    def do_blink_alternate(self, t):
        """ Arbeiter Funktion für das alternierende blinken. Diese Funktion wird in
            einem Thread ausgeführt und lässt die LEDs blinken. Diese Funktion wird
            von start_blink_alternate aufgerufen und sollte nicht manuel aufgerufen
            werden.

            Args:
                t(float): Die Zeit (s) nach der vom einen auf das andere LED gewechselt
                    wird.
        """
        while self.should_blink:
            self.leds["red"].turn_on()
            time.sleep(t)
            self.leds["red"].turn_off()
            self.leds["yellow"].turn_on()
            time.sleep(t)
            self.leds["yellow"].turn_off()

    def start_blink_single(self, color="red", t=0.5):
        """ Lässt die spezifizierte LED mit einer bestimmten Frequenz blinken..

            Args:
                color(string): 'red' oder 'yellow', welche der LEDs blinken soll.
                t(float): Die Zeit (s) welche die LED an bzw. aus bleibt.
        """
        if self.blink_thread:
            self.stop_blink()
        self.should_blink = True
        self.blink_thread = threading.Thread(target=self.do_blink_single, args=(color, t,))
        self.blink_thread.start()

    def do_blink_single(self, color, t):
        """ Arbeiter Funktion für blink single. Wird von start_blink_single
            aufgerufen.

            Args:
                color(string): 'red' oder 'yellow', welche der LEDs blinken soll.
                t(float): Die Zeit (s) welche die LED an bzw. aus bleibt.
        """
        while self.should_blink:
            self.leds[color].toggle()
            time.sleep(t)

    def stop_blink(self):
        """ Stop das blinken der LEDs. """
        self.should_blink = False
        if self.blink_thread:
            self.blink_thread.join()
        self.blink_thread = None

    def default_blink(self):
        if self.blink_thread:
            self.stop_blink()
        self.start_blink_single("red", 1)
