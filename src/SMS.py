# !/usr/bin/python3
# Filename: text.py
#-------------------------------------------------------------
#11.07.20 / / 23.01.21 / 24.01.21 / 28.01.21 / 04.02.21
#-------------------------------------------------------------
#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#                  Parameter Eingabe : Test
#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#---- Alarm Informations Text --------------------------------
AlarmA1 = "Alarm 1"
AlarmA2 = "Alarm 2"
AlarmA3 = "Alarm 3"
AlarmA4 = "Alarm 4"
AlarmA5 = "Alarm 5"
AlarmA6 = "Alarm 6"

#----- Info SMS Interval -------------------------------------
SMS_info = "+41792723336"

#----- Telefon Nummer der SIM-Karte für Zeit lesem notwendig -

SMS_NO = "0774332143"     # <<<<<<< ! ! ! <<<<<<<<<<<<<<<<<

#---- Zuordnung Tel.No zum Alarm -----------------------------
# Syntax beachten !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# Beispiel :    AlarmA1No = ["+41792723336", "+41797392291"]
#
AlarmA1No = ["+41792723336", "+41797392291"]
AlarmA2No = ["+41792723336"]
AlarmA3No = ["+41792723336"]
AlarmA4No = ["+41792723336"]
AlarmA5No = ["+41792723336"]
AlarmA6No = ["+41792723336"]

#----- SMS info senden ( Interval in Tagen) ------------------
SMSinfoStart = 15     # 0 bis 23, Stunde am Tag zum senden des Info SMS
               # Beispiel : 15  =  15:00:00 Uhr
SMSinfoInterval = 48  # Anzahl Stunden von SMS zu SMS

#---- Zentralennummer ----------------------------------------
ZNo = "+41794999000"          # M-Budget (Swisscom) 
#   ZNo = "+41765980000"          # Sunrise
#   ZNo = "+41787777070"          # Salt
#-------------------------------------------------------------

#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

# Ich habe probiert dir ein paar Kommentare im Code zu lassen. Ich hoffe das hilft dir,
# ein bisschen zu verstehen, wo du etwas besser machen könntest.
# Erst noch ein genereller Kommentar:
# In python gibt es eine Konvention, wie variablen etc. bennant werden sollen, die heisst PEP8
# Wichtig ist vorallem folgendes:
# Variablen werden immer klein geschrieben, und wenn sie aus mehreren Wörtern bestehen mit _ getrennt
# bsp: my_multi_word_variable = "Hello World"
# Ausnahme: Classen Namen werden gross geschrieben
# bsp: class MyAwsomeClass:
# Manche Leute schreiben auch globale Konstanten mit mit nur GROSSBUCHSTABEN
# bsp: SMSINFOSTART = 15
# Ich finde es lohnt sich sehr, dieser Konvention zu folgen. Es macht den Code viel lehserlicher und es
# ist auch einfacher Code von anderen Leuten zu lesen, wenn alle die gleichen Standarts benutzen.

import os
import sys
import time
import serial
import RPi.GPIO as GPIO
import shutil

from pathlib import Path

ser = serial.Serial("/dev/ttyS0",  115200, timeout=5)
ser.flushInput()

#Hier würde sich vielleicht ein dictionary anbieten um das ein bisschen ordenlicher zu gestalten
#pins = {"A1": 5, "A2": 6, "A3": 13, "A4": 19, "A5": 26, "A6": 21}
#Du kannst dann so wieder drauf zugreifen:
#pins["A2"]
PinA1 = 5
PinA2 = 6
PinA3 = 13
PinA4 = 19
PinA5 = 26
PinA6 = 21

PinSW1 = 24       # nc
PinSW2 = 25       # STOP

PinROT = 20       # LED
PinGELB = 16      # LED

PinFM = 4        # flight-mode
PinPWR = 12       # power-key

#Das würde ich in eine funktion tun
#-------------------------------------------------------------
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
#-------------------------------------------------------------
GPIO.setup(PinA1, GPIO.IN)  #Alarm : A1
GPIO.setup(PinA2, GPIO.IN)  #Alarm : A2
GPIO.setup(PinA3, GPIO.IN)  #Alarm : A3
GPIO.setup(PinA4, GPIO.IN)  #Alarm : A4
GPIO.setup(PinA5, GPIO.IN)  #Alarm : A6
GPIO.setup(PinA6, GPIO.IN)  #Alarm : A5
GPIO.setup(PinSW1, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # nc
GPIO.setup(PinSW2, GPIO.IN, pull_up_down=GPIO.PUD_UP)  # STOP
#-------------------------------------------------------------
GPIO.setup(PinPWR, GPIO.OUT)    #  Power-key
GPIO.setup(PinFM, GPIO.OUT)     #  Flight-mode
GPIO.setup(PinROT, GPIO.OUT)    #  LED 1
GPIO.setup(PinGELB, GPIO.OUT)   #  LED 2
#-------------------------------------------------------------
GPIO.output(PinFM, GPIO.LOW)
GPIO.output(PinPWR, GPIO.LOW)
GPIO.output(PinROT, GPIO.LOW)
GPIO.output(PinGELB, GPIO.LOW)
#-------------------------------------------------------------
#Es würde sich anbieten, eine Alarm Classe zu machen, du hast sonst sehr viele einzelne variablen im Code.
#Das macht den code weniger gut lehsbar und macht es schwierig den code zu erweitern (noch mehr alarme etc).
#Classen erlauben dir, mehrere attribute eines alarms zusammenfassen.
#Siehe meine Implementation da mache ich das so.
#Wenn du nicht classen benutzen möchtest, kannst du auch einen doppel dictionary benutzen
#bsp: alarms = {1:{"sent": False, "pin": 5, "text": "Alarm 1", "number": "0761349842",
#               2:{"sent": False, "pin": 6, "text": "Alarm 2", "number": "0791847361",}
#alarms[1]["text"] -> "Alarm 1"
AlarmA1Sent = 0
AlarmA2Sent = 0
AlarmA3Sent = 0
AlarmA4Sent = 0
AlarmA5Sent = 0
AlarmA6Sent = 0
#-------------------------------------------------------------
#Gib den variablen bedeutungsvolle namen, für mich ist so z.b. komplet unklar was das hier ist. Der code wird nicht schneller
#weil die variable namen kürzer sind ;)
#Ausserdem sehe ich, das du diese Variablen unten überschreibst. In python muss man variablen nicht zu erst deklarieren.
A1 = 0
A2 = 0
A3 = 0
A4 = 0
A5 = 0
A6 = 0
#-------------------------------------------------------------
#Ich würde empfehlen True, False zu verwenden statt integer. Das ist besser lehsbar und effizienter.
#Wenn du z.b. das hier machst: if 1   dann wird python zuerst den integer in einen bool umwandeln (type conversion)
#und erst dann, das if statement auswerten.. Also macht es quasi if bool(1). Also besser grad bool verwenden.
STOP = 1
#----- SMS info interval -------------------------------------
Tx = 0
Tcount = 0
SMSinfoStarted = 0
SMSinfoText = "Alarm-System aktiv"
SMSfail = 0 # globale Variable - nach Fehler sendSMS wiederholen
#-------------------------------------------------------------
LogFile = "log000.txt"     # 3 Buchstabe + "000" + ".txt" !!!
OutFile = "out000.txt"     # 3 Buchstabe + "000" + ".txt" !!!
#xxxxx PWRon xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
def PWRon (PinPWR, PinFM):
    GPIO.setwarnings(False)
    GPIO.output(PinROT, GPIO.HIGH)     #    LED
    GPIO.output(PinGELB, GPIO.HIGH)    #    LED
    time.sleep(0.1)
    GPIO.output(PinPWR,GPIO.LOW)
    GPIO.output(PinFM,GPIO.LOW)
    time.sleep(1)
    GPIO.output(PinPWR,GPIO.HIGH)
    GPIO.output(PinFM,GPIO.LOW)
    time.sleep(2)
    GPIO.output(PinPWR,GPIO.LOW)
    GPIO.output(PinFM,GPIO.LOW)
    PRGstop(20,"PowerOn")           # <<<<<<<<<<< Sleep-Time
    ser.flushInput()
    GPIO.output(PinROT, GPIO.LOW)     #    LED
    GPIO.output(PinGELB, GPIO.LOW)    #    LED
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#xxxxx PWRoff xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
def PWRoff ():
    GPIO.output(PinROT, GPIO.HIGH)     #    LED
    GPIO.output(PinGELB, GPIO.HIGH)    #    LED
    GPIO.setwarnings(False)
    time.sleep(0.1)
    GPIO.output(PinPWR,GPIO.LOW)
    GPIO.output(PinFM,GPIO.LOW)
    time.sleep(1)
    GPIO.output(PinPWR,GPIO.HIGH)
    GPIO.output(PinFM,GPIO.LOW)
    time.sleep(4)
    GPIO.output(PinPWR,GPIO.LOW)
    GPIO.output(PinFM,GPIO.LOW)
    PRGstop(16,"PowerOff")           # <<<<<<<<<<< Sleep-Time
    ser.flushInput()
    GPIO.output(PinROT, GPIO.LOW)     #    LED
    GPIO.output(PinGELB, GPIO.LOW)    #    LED
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#xxxxx Programm Stoppen mit Taste xxxxxxxxxxxxxxxxxxxxxxxxxxxx
def PRGstop(Nmax, T):
    N = 0
    GPIO.output(PinROT, GPIO.HIGH)     #    LED
    GPIO.output(PinGELB, GPIO.HIGH)    #    LED
    print(T,"<",N,end=".")
    while N < Nmax :
        STOP = PinINinv(PinSW2)
        if STOP == 0 :
            GPIO.output(PinROT, GPIO.HIGH)     #    LED
            GPIO.output(PinGELB, GPIO.HIGH)    #    LED
            time.sleep(0.5)
            GPIO.output(PinROT, GPIO.LOW)     #    LED
            GPIO.output(PinGELB, GPIO.LOW)    #    LED
            PowerSaveOff()            
            time.sleep(0.5)
            sys.exit(0)
        time.sleep(1)
        N = N + 1
        print(N,end=".")
    print(">")
    GPIO.output(PinROT, GPIO.LOW)     #    LED
    GPIO.output(PinGELB, GPIO.LOW)    #    LED

#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#xxxxx power save on xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
#---- nach neustart sind die anschlüsse wieder frei geschaltet
def PowerSaveOn():
    os.system("echo '1-1' | sudo tee /sys/bus/usb/drivers/usb/unbind > /dev/null")
    os.system("sudo /opt/vc/bin/tvservice -o > /dev/null")
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#xxxxx power save off xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
# D:\90 UTILITIES\Raspberry\Dok\Stromsparen\HOW TO SAVE POWER.pdf
def PowerSaveOff():
    os.system("echo '1-1' | sudo tee /sys/bus/usb/drivers/usb/bind > /dev/null")
    os.system("/opt/vc/bin/tvservice -p > /dev/null")
    os.system("sudo /bin/chvt 6; sudo /bin/chvt 7 > /dev/null")
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#xxxxx file exist xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#Ein cooler tipp: Also dass hier nennt man slicing my_string[3:5]. Wenn du hier eine negative zahl gibst
#sagst du das nte element von hinten. Wenn du keine Zalh gibst nach dem doppelpunk heisst das, bis zum Schluss (oder vom
#Anfang an).
#bsp: "out000.txt"[-4:] -> ".txt"
#bsp: "out000.txt"[:-4] -> "out000"
#So ist es dann auch egal wie lange der filename ist, du wirst immer genau die endung abschneiden.
#Du kanns überigens noch eine dritte zahl geben, die dann sagt in wie grossen Schritten es drüber geht
#bsp: "out000.txt"[::2] -> "lg0.x"
#Und noch ein fun fakt ;): "Peter"[-1::-1] -> "reteP"
#Man kann die file endung überigens auch sehr gut mit der split methode entfehrnen:
#bsp: file_name, file_ending = "out000.txt".split(".")

#du machst das ziemlich kompliziert...
def FILE_EXIST(F):
    my_file= Path("/home/pi/" + F)
    while my_file.is_file():
        N = ("000" + str(int((F[3:6])) + 1)) #Da ist eine Klammer zu viel um F[3:6] ;)
        L = len(N)  #Mit dem syntax, den ich dir oben gezeigt habe, brauchst du das nicht mehr
        F = F[0:3] + N[L-3:L]
        my_file = Path("/home/pi/" + F + ".txt")
    print("<<<<<  log file : ",  my_file)
    return (my_file) #Achtung diese Klammer brauchst du nicht, wenn du pech hast macht es wegen der Klammer eine Type conversion
                     #zu einer Tupple


#Hier eine simplere form
def file_exist_valentin():
    counter = 0
    my_file = Path("out0.log")
    while my_file.is_file():
        Path(my_file = "out" + str(counter) + ".log")
        counter += 1
    return my_file
        
def create_log_file():
    """ Wäre es nicht besser einfach einen Zeitstempel zu nehmen um dem file einen einzigartigen Name zu geben?
        Ist ja sogar noch eine nützliche information, von wann das logfile ist.
    """
    time_stamp = "-".join([ str(el) for el in time.localtime()[:5] ])
    return time_stamp + ".log"


#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#xxxxx send SMS xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
def sendSMS(recipient, message, Pro):
    print(">>>>> send SMS", recipient)
    send_at("AT+CMGF=1","OK",1)                   #<<<<<<<<<<1
#    print("Sending Message")
    answer = send_at("AT+CMGS=\""+recipient+"\"",">",2) #<<<<2
    if 1 == answer:
        ser.write(message.encode())
        ser.write(b'\x1A')
        answer = send_at('','OK',20)              #<<<<<<<<<<3
        if 1 == answer:
#            print('send successfully')
             time.sleep(0.1)     # nur als dummy
        else:
            print('----- error')
    else:
        print('===== error%d'%answer)
#-----------------------------------------------------------
    PRGstop(20, "warten nach SMS senden")
#-----------------------------------------------------------
    if Pro == 1 :
        Protokoll(recipient, message)
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#xxxxx send at xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
def send_at(command,back,timeout):
    global SMSfail
    SMSfail = 0
    rec_buff = ''
    try :
        ser.write((command+'\r\n').encode())
        time.sleep(timeout)
        if ser.inWaiting():
            time.sleep(0.01 )
            rec_buff = ser.read(ser.inWaiting())
        if back not in rec_buff.decode():
            print(command + ' ERROR')
            print(command + ' back:\t' + rec_buff.decode())
            return 0
        else:
            return 1
    except :
        SMSfail = 1
        return 0
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#xxxxx SMS lesen xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
def readSMS ():
    if(ser.isOpen() == False):
        ser.open()
    ser.write(b'AT+CSCA="+41794999000"' + b'\r\n')
                     # SWISSCOM Zentralennummer
    time.sleep(1)
    ser.write(b'AT+CMGF=1' + b'\r\n')
    time.sleep(1)
    ser.write(b'AT+CMGL="ALL"' + b'\r\n')
    out = ''
    outx = ''
    time.sleep(1)
    while ser.inWaiting() > 0:
        outx = ser.read(1).decode('iso-8859-1')
        out = out + outx
    return out
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#xxxxx SMS löschen xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
def GSM_CLEAR ():
    if(ser.isOpen() == False):
        ser.open()
    ser.write(b'AT+CMGD=1,4' + b'\r\n')
    out = ''
    outx = ''
    time.sleep(1)
    while ser.inWaiting() > 0:
        outx = ser.read(1).decode('iso-8859-1')
        out = out + outx
    time.sleep(5)
    return out
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
def GSM_TIME():
    #Man sollte es möglichst vermeiden globale variablen zu declarieren. Das
    #kann zu grossen Problemen führen. Hier wäre es sauberer, wenn du am
    #am Schluss einfach return SMSfail machst.
    global SMSfail
    sendSMS(SMS_NO,"Zeitlesen",0)
    SMS = readSMS()
    P=SMS.find("/")    #   position "/"
    T=SMS[P-2:P+15]    #   zeit

    JAHR=str(T[0:2])
    MONAT=str(T[3:5])
    TAG=str(T[6:8])
    STUNDEN=str(T[9:11])
    MINUTEN=str(T[12:14])
    SEKUNDEN=str(T[15:17])

    ZEIT=MONAT+TAG+STUNDEN+MINUTEN+JAHR
    if len(ZEIT) < 10 : SMSfail = 1 
    print("<<<<< Zeit via SMS : ", ZEIT, " (MM,TT,hh,mm,JJ)  >>>> ", SMSfail)

    command='sudo date %s' %ZEIT
    os.system(command)
    time.sleep(20)    #  warten das raspi zeit übernimmt

    #Dieses Statement macht so garnichts. Return ohne argument gibt einfach None zurück.
    #Das passiert auch automatisch wenn die Funktion durch alle Anweisungen durchgegangen ist.
    #Wie oben erwähnt solltest du hier return SMSfail machen.
    return
            
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#xxxxx GSM SIM status xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
def GSM_SIM ():
    if(ser.isOpen() == False):
        ser.open()
    ser.write(b'AT+CPIN?' + b'\r\n')
    out = ''
    time.sleep(1)
    while ser.inWaiting() > 0:
        out += ser.read(1).decode()
    out = out.replace("\r\n", "/")
    READY = 1 if ("READY" in out) else 0
    return READY
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#xxxxx GSM Network registration xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
def GSM_NET ():
    if(ser.isOpen() == False):
        ser.open()
    ser.write(b'AT+CREG?' + b'\r\n')
    out = ''
    time.sleep(1)
    while ser.inWaiting() > 0:
        out += ser.read(1).decode()
    out = out.replace("\r\n", "/")
    READY = 1 if ("+CREG: 0,1" in out) else 0
    return READY
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#xxxxx Signal Stärke xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
def GSM_SIGNAL ():
    if(ser.isOpen() == False):
        ser.open()
    ser.write(b'AT+CSQ' + b'\r\n')
    out = ''
    time.sleep(1)
    while ser.inWaiting() > 0:
        out += ser.read(1).decode()
    out = out.replace("\r", "/")
    out = out.replace("\n", "/")
    return out
#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#xxxxx entprellen / entstören xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
def PinIN(Pin):
    P1 = 0 #Du musst das nicht zu erst deklarieren, das kannst du weglassen du beschreibes es ja später eh wieder.
    #Was du hier machst ist redundant. GPIO.input() wird dir immer 1 oder 0 zurück geben
    #Der conditional if GPIO.input(Pin) wird immer dann erfüllt wenn GPIO.input(Pin) 1 returned.
    #Du beschreibst dann P0 mit 1, dabei returned GPIO.input(Pin) das ja schon.
    #Also du prüfst ob es 1 ist und wenn ja schreibst du 1 in eine variable. Du kanns einfach direkt den Output in eine
    #Variable schreiben.
    P0 = 1 if GPIO.input(Pin) else 0   # kurzform des if befehles
    if P0 == 1 :
        time.sleep(0.2)
        P1 = 1 if GPIO.input(Pin) else 0 # kurzform des if befehles
    #Das hier besser mit einer logic operation machen zb. return P0 and P1
    #Du musst es auch nicht erst noch in eine variable schreiben, wenn du es ja nacher eh gerade wieder returnst
    #Das ist ineffizient, weil der computer dann extra Ram aloziert für diese neue variable.
    PinOUT =  1 if P0 + P1 == 2 else 0
    return PinOUT # einfacher: return P0 and P1

#ich würde es so machen
def pin_in_valentin(pin):
    p0 = GPIO.input(pin)
    time.sleep(0.2)
    p1 = GPIO.input(pin)
    return p0 == p1 #wenn du es so machst, brauchst du auch nicht eine extra inv funktion

#noch ein bisschen effizienter aber schlechter lesbar
def pin_in_valentin2(pin)
    p0 = GPIO.input(pin)
    time.sleep(0.2)
    return p0 == GPIO.input(pin)
    

#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

#xxxxx entprellen / entstören xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
#Die funktion kannst du dir sparen, wenn du es machst, wie bei pin_in_valentin
def PinINinv(Pin):
    P1 = 0
    P0 = 1 if GPIO.input(Pin) else 0   # kurzform des if befehles
    if P0 == 0 :
        time.sleep(0.2)
        p1 = 1 if GPIO.input(Pin) else 0   # kurzform des if befehles
    PinOUT =  0 if P0 + P1 == 0 else 1
    return PinOUT


#xxxxx Protokoll schreiben xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
def Protokoll(r, m) :
    file = open(DATA_FN, "a")
    file.write(r + "  " + m)
    file.write("\r\n")
    file.close()

#xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
#=============================================================
#                      Hauptprogramm
#=============================================================
print ("<<<<<<<<<<  start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
#----- Log file Name generieren ------------------------------
LogFile = FILE_EXIST(LogFile)
OutFile = FILE_EXIST(OutFile)
#----- ausgabe umleiten --------------------------------------
save_stdout = sys.stdout
flog = open(LogFile, "w")
sys.stdout = flog
#----- Stabilisierung ----------------------------------------
PRGstop(5,"Prg-Start")

#----- Schnittstellen einschalten ----------------------------
print("<<<<< PowerSaveOff")
PowerSaveOff()

#----- GSM reset ---------------------------------------------
print("<<<<< Power On")
PWRon(PinPWR, PinFM)             #  24 sek
print("<<<<< Status PowerOn GSM_NET : ", GSM_NET())

#----- Test SMS senden ---------------------------------------
print("<<<<< send Dummy SMS")
sendSMS (SMS_info, "dummy",0)

#----- Zeit holen und stellen --------------------------------
print("<<<<< SMS löschen und Zeit holen   SMSfail :", SMSfail)
GSM_CLEAR()
#SMSfail = GSM_TIME() wie oben erwähnt solltest du SMSfail returnen
GSM_TIME()
if SMSfail == 1 :
    print("<<<<< SMS löschen und Zeit holen Wiederholung   SMSfail :", SMSfail)
    GSM_CLEAR
    GSM_TIME()
SMSfail = 0 #Wieso setzt du das hier auf 0?

#ein bisschen einfacher und besser anpassbar wäre:
#attempts=2
#for i in range(attempts):
#   print("SMS löschen und Zeit holen, Versuch nummer", i+1)
#   GSM_CLEAR()
#   SMSfail = GSM_TIME()
#   if not SMSfail:
#        break

#Du löst ja gar nichts aus wenn das Zeitholen fehlschlägt... Ist das Zeit holen wirklich nötig?

#----- Datum und Zeit lesen ----------------------------------
lt = time.localtime()
datetime =(time.strftime("%d.%m.%Y %H:%M:%S", lt))

#----- Name Protokoll Datei definieren -----------------------
DATA_FN = (time.strftime("DATA-%Y-%m-%d-%H%M", lt)) + (".txt")
print ("<<<<< Protokoll : ", DATA_FN)

#----- SMS Info - Programm gestartet -------------------------
print("<<<<< send SMS Info Start")
INFO = datetime + "\r\n" + "GSM Alarm gestartet \r\n >>> " + str(GSM_SIGNAL()) + "\r\n"
sendSMS (SMS_info, INFO,1)
if SMSfail == 1 :
    print("<<<<< send SMS Info Start Wiederholung")
    sendSMS (SMS_info, INFO,1)
print("<<<<< send SMS Info Start gesendet", INFO)

#-------------------------------------------------------------
LED_COUNT = 0
GPIO.output(PinGELB, GPIO.HIGH)
GPIO.output(PinROT, GPIO.LOW)

#------   USB & LAN & HDMI  ausschalten (Stromsparen)  -------
PowerSaveOn()

#------  log file stoppen / starten  -------------------------
sys.stdout = save_stdout
flog.close()

save_stdout = sys.stdout
fout = open(OutFile, "w")
sys.stdout = fout

#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
#                            LOOP
#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
#Generel würde man in python eigentlich eher booleans brauchen anstatt 1 und 0
#Ausserdem liest es sich ein bisschen komisch wenn du stop als 1 also true definierst.
#das würde ja eigentlich heissen, das das programm stoppen soll. ich würde es so mache:
#stop = False
#while not stop:
while STOP == 1 :
    lt = time.localtime()
    datetime =(time.strftime("%d.%m.%Y %H:%M:%S", lt)) + "\r\n"

#ooooo Anzeige Alarm aktiv ooooooooooooooooooooooooooooooooooo
    LED_COUNT = LED_COUNT + 1
    if LED_COUNT < 0:
        GPIO.output(PinROT, GPIO.HIGH)
        GPIO.output(PinGELB, GPIO.HIGH)
    if LED_COUNT > 0:
        GPIO.output(PinROT, GPIO.LOW)
        GPIO.output(PinGELB, GPIO.HIGH)
    if LED_COUNT > 20000:
        LED_COUNT = -20000

#----- Status prüfen und notfalls GSM PWRon ------------------
#        READY = GSM_NET()
#        while READY == 0:
#            PWRon(PinPWR, PinFM)
#            time.sleep(1)
#            READY = GSM_NET()
#            print("wiederholung GSM_NET : ", READY)

#----- SMS info in Intervallen senden ------------------------
    TimeH = int(time.strftime("%H", lt))
    if SMSinfoStart == TimeH and SMSinfoStarted == 0:
        SMSinfoStarted = 1
        Tcount = -1
        sendSMS (SMS_info, datetime + SMSinfoText,1)
        print("<<<<< SMSinfoStarted", datetime)
    if Tx != TimeH and SMSinfoStarted == 1:
        Tx = TimeH
        Tcount += 1
        if Tcount == SMSinfoInterval :
            Tcount = 0
            sendSMS (SMS_info, datetime + SMSinfoText,1)
            print("<<<<< SMSinfo", datetime)

#ooooo Eingänge lesen  ooooooooooooooooooooooooooooooooooooooo
    A1 = PinIN(PinA1)
    A2 = PinIN(PinA2)
    A3 = PinIN(PinA3)
    A4 = PinIN(PinA4)
    A5 = PinIN(PinA5)
    A6 = PinIN(PinA6)
    STOP = PinINinv(PinSW2)
#ooooo  Alarm 1 auslösen ooooooooooooooooooooooooooooooooooooo
    if A1 == 1:
        if AlarmA1Sent == 0:
            AlarmA1Sent = 1
            I = 0
            while I < len(AlarmA1No) :
                sendSMS (AlarmA1No[I], datetime + AlarmA1,1)
#-------------------------------------------------------------
                if SMSfail == 1 :
                    print("**************  wiederholung Alarm A1")
                    sendSMS (AlarmA1No[I], datetime + AlarmA1,1)
#-------------------------------------------------------------
                print("A1", I, AlarmA1No[I], datetime)
                time.sleep(3)
                I = I + 1
    else:
        AlarmA1Sent = 0
#ooooo  Alarm 2 auslösen ooooooooooooooooooooooooooooooooooooo
    if A2 == 1:
        if AlarmA2Sent == 0:
            AlarmA2Sent = 1
            I = 0
            while I < len(AlarmA2No) :
                sendSMS (AlarmA2No[I], datetime + AlarmA2,1)
#-------------------------------------------------------------
                if SMSfail == 1 :
                    print("**************  wiederholung Alarm A2")
                    sendSMS (AlarmA2No[I], datetime + AlarmA2,1)
#-------------------------------------------------------------
                print("A2", I, AlarmA2No[I], datetime)
                time.sleep(3)
                I = I + 1
    else:
        AlarmA2Sent = 0
#ooooo  Alarm 3 auslösen ooooooooooooooooooooooooooooooooooooo
    if A3 == 1:
        if AlarmA3Sent == 0:
            AlarmA3Sent = 1
            I = 0
            while I < len(AlarmA3No) :
                sendSMS (AlarmA3No[I], datetime + AlarmA3,1)
#-------------------------------------------------------------
                if SMSfail == 1 :
                    print("**************  wiederholung Alarm A3")
                    sendSMS (AlarmA3No[I], datetime + AlarmA3,1)
#-------------------------------------------------------------
                print("A3", I, AlarmA3No[I], datetime)
                time.sleep(3)
                I = I + 1
    else:
        AlarmA3Sent = 0
#ooooo  Alarm 4 auslösen ooooooooooooooooooooooooooooooooooooo
    if A4 == 1:
        if AlarmA4Sent == 0:
            AlarmA4Sent = 1
            I = 0
            while I < len(AlarmA4No) :
                sendSMS (AlarmA4No[I], datetime + AlarmA4,1)
#-------------------------------------------------------------
                if SMSfail == 1 :
                    print("**************  wiederholung Alarm A4")
                    sendSMS (AlarmA4No[I], datetime + AlarmA4,1)
#-------------------------------------------------------------
                print("A4", I, AlarmA4No[I], datetime)
                time.sleep(3)
                I = I + 1
    else:
        AlarmA4Sent = 0
#ooooo  Alarm 5 auslösen ooooooooooooooooooooooooooooooooooooo
    if A5 == 1:
        if AlarmA5Sent == 0:
            AlarmA5Sent = 1
            I = 0
            while I < len(AlarmA5No) :
                sendSMS (AlarmA5No[I], datetime + AlarmA5,1)
#-------------------------------------------------------------
                if SMSfail == 1 :
                    print("**************  wiederholung Alarm A5")
                    sendSMS (AlarmA5No[I], datetime + AlarmA5,1)
#-------------------------------------------------------------
                print("A5", I, AlarmA5No[I], datetime)
                time.sleep(3)
                I = I + 1
    else:
        AlarmA5Sent = 0
#ooooo  Alarm 6 auslösen ooooooooooooooooooooooooooooooooooooo
    if A6 == 1:
        if AlarmA6Sent == 0:
            AlarmA6Sent = 1
            I = 0
            while I < len(AlarmA6No) :
                sendSMS (AlarmA6No[I], datetime + AlarmA6,1)
#-------------------------------------------------------------
                if SMSfail == 1 :
                    print("**************  wiederholung Alarm A6")
                    sendSMS (AlarmA6No[I], datetime + AlarmA6,1)
#-------------------------------------------------------------
                print("A6", I, AlarmA6No[I], datetime)
                time.sleep(3)
                I = I + 1
    else:
        AlarmA6Sent = 0
        
#----- Programm mit Hilfe ext. Datei stopppen ---------------    
    my_file= Path("/home/pi/ProgStopp.txt")
    if my_file.is_file(): STOP = 0
#ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
#-------------------------------------------------------------
GPIO.output(PinGELB, GPIO.LOW)    #    LED
GPIO.output(PinROT, GPIO.LOW)    #    LED

#----- Stop Eingang überprüfen und Programm stoppen ----------
time.sleep(1)
STOP = PinINinv(PinSW2)
time.sleep(1)

#----- Stop Eingang überprüfen und raspi ausschalten ---------
n = 0
while n <= 30 :
    STOP = PinINinv(PinSW2)
    time.sleep(0.1)
    GPIO.output(PinROT, GPIO.LOW)    #    LED
    time.sleep(0.1)
    GPIO.output(PinROT, GPIO.HIGH)    #    LED
    n = n + 1
    if STOP == 0 :
        PowerSaveOff()
        GPIO.cleanup()
        sys.stdout = save_stdout
        fout.close()
        sys.stderr = save_stderr
        ferr.close()
        print("<<<<< shutdown", datetime)
        os.system("sudo shutdown -h now")  #  raspberry power off
        
#-----  Programm stoppen  ------------------------------------
GPIO.output(PinGELB, GPIO.LOW)
GPIO.output(PinROT, GPIO.LOW)
GPIO.cleanup()

sys.stdout = save_stdout
fout.close()

PowerSaveOff()

print("<<<<< Prg-end   >>>> ", datetime)
time.sleep(5)

