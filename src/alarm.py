class Alarm:
    def __init__(self, pin, text, numbers):
        """ Diese Klasse dient als simpler Kontainer für Alarme.

            Args:
                pin(int): Die Nummer des Input Pins, welcher den entsprechenden
                    Alarm auslöst.
                text(string): Der Text welcher bei diesem Alarm versendet werden
                    soll.
                numbers(list of string): Die Telefon Nummern an welche der Alarm
                    versendet werden soll.
        """
        self.pin = pin
        self.text = text
        self.numbers = numbers
