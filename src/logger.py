import logging
import time

from config import *


class Logger:
    def __init__(self, name):
        """ Diese Klasse wird verwendet um Status Meldungen zu schreiben. Die
            Nachrichten werden einerseits in die Konsole geschrieben und
            andererseits in ein Logfile. Es können verschiedene Levels definiert
            werden ab denen beispielsweise Nachrichten in den Log geschrieben werden
            sollen. Es gibt 5 Levels: Debug, Info, Warning, Error und Critical. Der
            Level kann im config file spezifiziert werden. Jedes Modul initialisiert
            einen eigenen Logger, dadurch ist immer klar von wo eine Nachricht
            geschrieben wurde.

            Args:
                name(string): Der Name des Moduls für welches die entsprechende
                    Logger Instanz verwendet werden soll.
        """
        logging_format="%(levelname)s %(name)s -%(asctime)s- %(message)s",
        self.logger = logging.getLogger(name)
        self.logger.setLevel(logging.DEBUG)
        self.logger.propagate = False

        console_handler = logging.StreamHandler()
        console_handler.setLevel(CONSOLE_LEVEL)
        console_format = logging.Formatter("%(levelname)s '%(name)s': %(message)s")
        console_handler.setFormatter(console_format)

        file_handler = logging.FileHandler("logs/" + self.generate_filename())
        file_handler.setLevel(FILE_LEVEL)
        file_format = logging.Formatter("%(levelname)s '%(name)s' %(asctime)s: %(message)s")
        file_handler.setFormatter(file_format)

        self.logger.addHandler(console_handler)
        self.logger.addHandler(file_handler)

    def generate_filename(self):
        """ Generiert den Filenamen für das Logfile. Das Schema für den Filenamen
            ist: Jahr-Monat-Tag-Stunde-Minute.log

            Returns:
                file_name(string): Der Filename
        """
        return "-".join([ str(el) for el in time.localtime()[:5] ]) + ".log"

    def get(self):
        """ Gibt die brauchbare Instanz des Loggers zurück."""
        return self.logger


if __name__ == "__main__":
    my_logger = Logger("test").get()
    my_logger.debug("debug")
    my_logger.info("info")
    my_logger.warning("warning")
    my_logger.critical("critical")
