#!/usr/bin/python3

import serial
import time

import logger

class SIM7600:
    def __init__(self, rpi):
        """ Diese Klasse repräsentiert den SIM7600 Chip. die Klasse bietet
            Funktionen, um diesen anzusteuern. So wird die Stromzufuhr an den Chip
            von hier aus kontrolliert. Ausserdem können SMS Nachricthen verschickt
            werden.

            Args:
                rpi(rpi.RPI): Die Instanz des RPI Objects.
        """
        self.rpi = rpi

        self.logger = logger.Logger("SIM7600").get()

        self.state = "IDLE"
        self.running = True
        self.has_power = False
        self.queue = list()

        self.serial = serial.Serial("/dev/ttyS0",115200)
        self.serial.flushInput()

        self.logger.info("Instanz gestarted")

    def stop(self):
        """ Stopt das Modul. Falls das Modul angeschaltet ist, wird es
            heruntergefahren.
        """
        self.logger.info("Instanz wird gestoppt")
        self.running = False
        if self.has_power:
            self.toggle_power()

        self.logger.info("Instantz beendet")

    def main_loop(self):
        """ Die Hauptschleife des Moduls. Hier wird eine Statemachine implementiert.
            Je nachdem in welchem State sich das Modul befindet, werden
            unterschiedliche Funktionen ausgeführt.
        """
        while self.running:
            if self.state == "IDLE":
                if self.queue and not self.has_power:
                    self.change_state("POWERING_UP")
                    self.toggle_power()
                    self.has_power = True
                    self.change_state("READY")
                else:
                    time.sleep(0.2)


            elif self.state == "READY":
                if not self.queue:
                    self.change_state("POWERING_DOWN")
                    self.toggle_power()
                    self.has_power = False
                    self.change_state("IDLE")
                else:
                    self.change_state("SENDING")
                    self.send_msg(*self.queue[0])

            elif self.state == "SENDING":
                time.sleep(0.2)


    def change_state(self, new_state):
        """ Dies Funktion wird verwendet, um den State der internen Statemachine zu
            verändern.

            Args:
                new_state(string): Der gewünschte neue State.
        """
        self.logger.debug(f"Statemachine wechselt auf state {new_state}")
        self.state = new_state

    def toggle_power(self):
        """ Schaltet den Strom ein oder aus, je nachdem in welchem Zustand der
            Chip sich gerade befindet.
        """
        self.logger.debug("Wechsle Power State")

        self.rpi.gsm_pins["power"].turn_on()
        self.rpi.start_blink_single("yellow", 0.3)
        time.sleep(3)
        self.rpi.gsm_pins["power"].turn_off()
        self.rpi.start_blink_single("yellow", 1)
        time.sleep(20)
        self.rpi.default_blink()

        self.logger.debug("Power State gewechselt")

    def send_greeting(self, number):
        year, month, day, hour, minute = time.localtime()[:5]
        signal_strength = self.get_signal_strength()
        if not signal_strength:
            self.toggle_power()
            self.has_power = True
            signal_strength = self.get_signal_strength()

        msg = "Alarm Server wurde gestarted\n"
        msg += "Zeit: {}:{} {}.{} {}\n".format(hour, minute, day, month, year)
        msg += signal_strength
        self.send(number, msg)
        self.change_state("READY")


    def send(self, number, msg):
        """ Dies Methode wird von anderen Modulen verwendet, um SMS Nachrichten zu
            verschicken. Die gewünschte Nachricht wird in der queue zwischen
            gespeichert un verschickt sobald der Chip bereit ist.

            Args:
                number(string): Die Telefon Nummer and die die Nachricht verschickt
                    werden soll.
                msg(string): Die Nachricht welche verschickt werden soll.
        """
        self.queue.append( (number, msg) )

    def send_at(self, cmd, response, timeout):
        """ Schickt einen AT befhel an das Gerät um das schicken einer Nachricht
            einzuleiten.

            Args:
                cmd(string): Der gewünschte AT Befehl.
                response(string): Der response Code der erwartet wird.
                timeout(float): Wenn das Schicken des AT Befehls nach dieser Zeit (s)
                    nicht erfolgreich war wird die Funktion abgebrochen.

            Returns:
                error_code(int): Code zeigt ob die Funktion erfolgreich beendet
                    wurde. 0 = Erfolgreich beendet, 1 = Unbekannter Fehler,
                    2 = Der Chip war wahrscheinlich ausgeschaltet.
        """
        rec_buff = str()
        self.serial.write( (cmd+"\r\n").encode() )
        time.sleep(timeout)
        if self.serial.inWaiting():
            time.sleep(0.01)
            rec_buff = self.serial.read(self.serial.inWaiting())
        else:
            self.logger.warning(f"Fehler, SIM7600 nicht bereit")
            return 2

        if response not in rec_buff.decode():
            self.logger.error(f"Fehler beim Senden der Nachricht {cmd}")
            return 1
        else:
            return 0

    def send_msg(self, number, msg):
        """ Sendet eine SMS Nachricht and die angegebene Adresse. Diese Methode
            wird vorallem Modul intern verwendet. Andere Module verschicken
            Nachrichten via SIM7600.send.

            Args:
                number(string): Die Telefon Nummer and die die Nachricht verschickt
                    werden soll.
                msg(string): Die Nachricht welche verschickt werden soll.

        """
        self.rpi.start_blink_alternate(0.5)
        self.logger.info(f"Sende Nachricht: '{msg}' an {number}")
        self.send_at("AT+CMGF=1", "OK", 1)
        answer = self.send_at('AT+CMGS="{}"'.format(number), ">", 2)
        if answer == 0:
            self.serial.write(msg.encode())
            self.serial.write(b'\x1A')
            answer = self.send_at('', 'OK', 20)
            if answer == 0:
                del self.queue[0]
                self.change_state("READY")
                self.logger.info("Nachricht wurde erfolgreich versandt")
            else:
                #im moment probiert er es einfach solang zu schicken bis es
                #funktioniert.
                self.change_state("READY")
                #del self.queue[0]
                self.logger.warning(f"Nachricht '{msg}' konnte nicht verschickte werden")
        elif answer == 2:
            self.logger.warning(f"Nachricht '{msg}' konnte nicht verschickte werden")
            self.has_power = False
            self.change_state("IDLE") #Causes power toggle
        else:
            #del self.queue[0]
            self.logger.warning(f"Nachricht '{msg}' konnte nicht verschickte werden")
            self.change_state("READY")
        self.rpi.default_blink()

    def receive_msg(self):
        """ Empfahngen einer Nachricht. Ungetestet und funktioniert wahrscheinlich
            nicht."""
        rec_buf = ""
        self.send_at("AT+CMGF=1", "OK", 1)
        self.send_at('AT+COMS="SM","SM","SM"', 'OK', 1)
        answer = self.send_at("AT+CMGR=1", "+CMGR:", 2)
        if answer == 1:
            if "OK" in rec_buff:
                self.logger.debug("Rückmeldungs Code OK")
        else:
            self.logger.debug(f"Rückmeldungs Code {answer}")
            return False
        return True

    def get_signal_strength(self):
        """ Erfragt die Signal Stärke des Handy Netzes. Die Funktion wird momentan
            nicht verwendet.
        """
        if not self.serial.isOpen():
            self.serial.open()
        self.serial.write(b"AT+CSQ"+b"\r\n")
        out = str()
        time.sleep(1)
        while self.serial.inWaiting() > 0:
            out += self.serial.read(1).decode()
        out = out.replace("\r", "/")
        out = out.replace("\n", "/")
        return out

if __name__ == "__main__":
    #gsm = SIM7600(12, 4)
    #gsm.power_on()
    #gsm.send_msg("0764154494", "hallo nochmal")
    #gsm.receive_msg()
    #gsm.power_down()
    pass
